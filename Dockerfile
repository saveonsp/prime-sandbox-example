arg version=3.9
from python:${version}

workdir /usr/src/app
copy main.py .

cmd ["python3", "main.py"]
