import json
from pprint import pprint
from operator import itemgetter
from urllib.request import urlopen, Request
from urllib.parse import urlencode
from urllib.error import HTTPError


# all API calls use POST
def request(url: str, headers, payload):
    """encode payload with application/x-www-form-urlencoded and POST"""
    data = urlencode(payload).encode()
    req = Request(url, headers=headers, data=data, method="POST")
    try:
        with urlopen(req) as res:
            return json.load(res)
    except HTTPError as e:
        err_s = e.read()
        try:
            err_msg = json.loads(err_s)
        except json.decoder.JSONDecodeError:
            err_msg = err_s
        err = Exception(f"{e.code}: {err_msg}")
        e.close()
        raise err from e


# base sandbox URL used in subsequent requests
base_url = "https://apisandbox.primetherapeutics.com"

# required headers for API
headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": "Basic 123456789abc",
}

# start by getting an access token via password_grant
payload = {
    "grant_type": "password",
    "username": "sandbox-user",
    "password": "sandbox-pw",
}

token_url = f"{base_url}/token"

try:
    body = request(token_url, headers, payload)
except Exception as e:
    err = Exception("failed to fetch token")
    raise err from e

# validate that all expected properties returned by request are present
keys = [
    "access_token",
    "refresh_token",
    "scope",
    "token_type",
    "expires_in",
]
assert all(k in body for k in keys), "missing key in reponse"
(access_token, refresh_token, scope, token_type, expires_in,) = itemgetter(
    *keys
)(body)
assert type(access_token) is str, "invalid access_token"

headers |= {
    "Authorization": f"Basic {access_token}",
}


search_url = f"{base_url}/R4/Coverage/_search"


def search(payload):
    try:
        return request(search_url, headers, payload)
    except Exception as e:
        err = Exception("search failed")
        raise err from e


# Basic Scenario: Search by Name and Member Number as of the Current Date
payload = {
    "beneficiary.given": "Jane",
    "beneficiary.family": "Doe",
    "beneficiary.birthdate": "1966-08-10",
    "beneficiary.identifier:of-type": "http://terminology.hl7.org/CodeSystem/v2-0203|MB|11111111101",
    "service-date": "eq2019-10-15",
    "_include": "Coverage:beneficiary",
}

pprint(search(payload))

payload = {
    "beneficiary.given:exact": "Jane",
    "beneficiary.family:exact": "DOE",
    "beneficiary.birthdate": "1966-08-10",
    "beneficiary.identifier:of-type": "http://terminology.hl7.org/CodeSystem/v2-0203|MB|11111111101",
    "beneficiary.gender": "http://hl7.org/fhir/administrative-gender|female",
    "service-date": "ge2018-01-01",
    "service-date": "le2019-12-31",
    "class-type:of-type": "http://terminology.hl7.org/CodeSystem/coverage-class|subclass|MAPD",
    "_include": "Coverage:beneficiary",
}

pprint(search(payload))
